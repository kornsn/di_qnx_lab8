#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <inttypes.h>
#include <errno.h>
#include <sys/neutrino.h>
#include <process.h>
#include "messaging.h"

// Module 2.
// Takes pid1, chid1 as arguments.
int main(int argc, char* argv[])
{
    int chid1, chid2, chid3, pid1, pid2, pid3, rcvid;
    char x1[14], x2[14], message[200];
    char* p3args[3];

    // Parsing command line arguments
    if (argc < 3) {
        puts(" P2: Illegal number of arguments");
        exit(-1);
    }

    pid2 = getpid();
    printf(" P2: Hello! My PID: %i\n", pid2);

    // Get chid1
    pid1 = atoi(argv[1]);
    chid1 = atoi(argv[2]);
    printf(" P2: PID1: %i, CHID1: %i\n", pid1, chid1);

    // Create channel
    chid2 = createChannel();
    printf(" P2: Channel created. CHID: %i\n", chid2);

    // Create P3
    itoa(getpid(), x1, 10);
    itoa(chid2, x2, 10);
    p3args[0] = "m3";
    p3args[1] = x1;
    p3args[2] = x2;
    p3args[3] = NULL;
    printf(" P2: Creating P3\n");
    pid3 = spawnv(P_NOWAIT, "m3", p3args);
    printf(" P2: Process 3 created. PID3: %i\n", pid3);

    // Send message to P1 with pid3
    itoa(pid3, x1, 10);
    sendMessage(pid1, chid1, x1);
    printf(" P2: Message with PID3 sent to P1\n");

    // Waiting for message "P3 loaded"
    printf(" P2: Waiting for message 'P3 loaded'\n");
    getMessage(chid2, message);
    printf(" P2: Message received. Text: %s\n", message);

    if (strcmp(message, "P3 loaded") != 0) {
        puts(" P2: Wrong message. Exit");
        exit(1);
    }

    // Send it to P1
    sendMessage(pid1, chid1, message);
    printf(" P2: Message recent to P1\n");

    // Get message with chid3
    printf(" P2: Waiting message from P3 with CHID3\n");
    getMessage(chid2, message);
    printf(" P2: Message received. Text: %s\n", message);

    chid3 = atoi(message);
    printf(" P2: CHID3: %i\n", chid3);

    // Send chid3 to P1
    sendMessage(pid1, chid1, message);
    printf(" P2: Message with CHID3 sent to P1\n");

    // Out "P2 LOADED"
    puts(" P2: P2 is loaded");

    // Waiting for stop signal
    printf(" P2: Waiting message with 'stop' signal\n");
    getMessage(chid2, message);
    printf(" P2: Message received. Text: %s\n", message);

    if (strcmp(message, "stop") == 0) {
        sendMessage(pid1, chid1, message);
        puts(" P2: Stop P2");
        exit(0);
    }
    else {
        printf(" P2: Unknown command %s\n", message);
        exit(-1);
    }
}