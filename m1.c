#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <inttypes.h>
#include <errno.h>
#include <sys/neutrino.h>
#include <process.h>
#include "messaging.h"

// Module 1 entry point.
int main(int argc, char* argv[])
{
    int chid1, chid2, chid3, pid1, pid2, pid3, rcvid;
    char x1[14], x2[14], message[200];
    char* p2args[4];

    pid1 = getpid();
    printf("P1: Hello! My PID: %i\n", pid1);

    // Create channel Ch1
    chid1 = createChannel();
    printf("P1: Channel created. Chid: %i\n", chid1);

    // Create process P2
    itoa(pid1, x1, 10);
    itoa(chid1, x2, 10);
    p2args[0] = "m2";
    p2args[1] = x1;
    p2args[2] = x2;
    p2args[3] = NULL;
    printf("P1: Creating P2\n");
    pid2 = spawnv(P_NOWAIT, "m2", p2args);
    printf("P1: Process P2 created. PID2: %i\n", pid2);

    // Listening for pid3
    printf("P1: Waiting message with PID of P3.\n");
    getMessage(chid1, message);
    printf("P1: Message received. Text: %s\n", message);

    pid3 = atoi(message);
    printf("P1: PID3: %i\n", pid3);

    // Listening for "P3 Loaded"
    printf("P1: Waiting message 'P3 loaded'\n");
    getMessage(chid1, message);
    printf("P1: Message received. Text: %s\n", message);

    if (strcmp(message, "P3 loaded") != 0) {
        puts("P1: Wrong message. Exit");
        exit(1);
    }

    // Listening for chid 3
    printf("P1: Waiting message with CHID3\n");
    getMessage(chid1, message);
    printf("P1: Message received. Text: %s\n", message);
    chid3 = atoi(message);
    printf("P1: CHID3: %i\n", chid3);

    // Send stop to P3
    strcpy(message, "stop");
    sendMessage(pid3, chid3, message);
    printf("P1: Message 'stop' sent to CHID3.\n");

    // Listening for "stop"
    printf("P1: Waiting message with 'stop' signal\n");
    getMessage(chid1, message);
    printf("P1: Message received. Text: %s\n", message);

    if (strcmp(message, "stop") == 0) {
        puts("P1: Stop P1");
        exit(0);
    }
    else {
        printf("P1: Unknown command %s\n", message);
        exit(-1);
    }
}