#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <inttypes.h>
#include <errno.h>
#include <sys/neutrino.h>
#include <process.h>
#include "messaging.h"

// Module 3.
// Takes pid2, chid2 as arguments.
int main(int argc, char* argv[])
{
    int chid1, chid2, chid3, pid2, pid3, rcvid;
    char x[14], message[200];

    // Parsing command line arguments
    if (argc < 3) {
        puts("  P3: Illegal number of arguments");
        return -1;
    }

    pid3 = getpid();
    printf("  P3: Hello! My PID: %i\n", pid3);

    // Get chid2
    pid2 = atoi(argv[1]);
    chid2 = atoi(argv[2]);
    printf("  P3: PID2: %i, CHID2: %i\n", pid2, chid2);

    // Create channel
    chid3 = createChannel();
    printf("  P3: Channel created. CHID: %i\n", chid3);

    // Send message "Loaded"
    strcpy(x, "P3 loaded");
    sendMessage(pid2, chid2, x);
    printf("  P3: Message 'P3 Loaded' sent to P2\n");

    // Send message with chid3
    itoa(chid3, x, 10);
    sendMessage(pid2, chid2, x);
    printf("  P3: Message with CHID3 sent to P2\n");

    // Waiting stop signal
    printf("  P3: Waiting message with 'stop' signal\n");
    getMessage(chid3, message);
    printf("  P3: Message received. Text: %s\n", message);

    if (strcmp(message, "stop") == 0) {
        // Send stop signal to P2
        sendMessage(pid2, chid2, "stop");
        puts("  P3: Message 'stop' sent to P2");
        puts("  P3: Stop P3");
        exit(0);
    }
    else {
        printf("  P3: Unknown command %s\n", message);
        exit(-1);
    }
}