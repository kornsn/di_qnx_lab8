#include <stdio.h>
#include <pthread.h>
#include <inttypes.h>
#include <errno.h>
#include <sys/neutrino.h>

// Creates channel and returns it's chid.
int createChannel()
{
    int chid;
    chid = ChannelCreate(0);
    return chid;
}

// Sends message to specified channel.
void sendMessage(int pid, int chid, char* message)
{
    char rmsg[20];
    long coid;
    int status;
    printf("   PID %i, CHID %i, MESSAGE: %s\n", pid, chid, message);

    coid = ConnectAttach(0, pid, chid, _NTO_SIDE_CHANNEL, 0);
    if (coid == -1) {
        printf("   Connection error. Exit\n");
        exit(1);
    }

    status = MsgSend(coid, message, 200, rmsg, sizeof(rmsg));
    if (status == -1) {
        printf("   Sending error. Exit\n");
        exit(1);
    }
}

// Gets message from specified channel.
void getMessage(int chid, char* message)
{
    int rcvid;
    rcvid = MsgReceive(chid, message, 200, NULL);
    MsgReply(rcvid, EOK, NULL, 0);
}
