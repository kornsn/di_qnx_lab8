// Creates channel and returns it's chid.
int createChannel();

// Sends message to specified channel.
void sendMessage(int pid, int chid, char* message);

// Gets message from specified channel.
void getMessage(int chid, char* message);
